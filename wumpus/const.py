# Constants for the agent actions.

WALK         = 1
ROTATE_LEFT  = 2
ROTATE_RIGHT = 3
FIRE         = 4
CLIMB        = 5
LOOT         = 6